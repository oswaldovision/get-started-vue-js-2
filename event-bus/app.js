var EventBus = new Vue;

Vue.component('app-icon', {
  template: '<span :class="cssClasses" aria-hidden="true"></span>',
  props: ['img'],
  computed: {
    cssClasses: function () {
      return 'glyphicon glyphicon-' + this.img
    }
  }
})

Vue.component('app-task',{
  template: '#task-template',
  props: ['task','index'],
  data: function(){
    return {
      editing : false,
      draft: ''
    }
  },
  created: function(){
    EventBus.$on('editing',function (index) {
      if (this.index != index){
        console.log('Descartando:', this.index);
        this.discart();
      }
    }.bind(this))
  },
  methods:{
    toogleStatus: function () {
      this.task.pending = !this.task.pending
    },
    edit: function () {
      console.log('Editando:', this.index);
      EventBus.$emit('editing', this.index);
      //FIX ME: implement
      /*
      this.tasks.forEach(function (task) {
        task.editing = false
      })
      */

      this.draft = this.task.description

      this.editing = true
    },
    update: function () {
      this.task.description = this.draft
      this.editing = false
    },
    discart: function () {
      this.editing = false;
    },
    remove: function () {
      this.$emit('remove',this.index)
    }
  }
})

var vm = new Vue({
  el: '#app',
  data: {
    new_task: '',
    tasks: [
      {
        description: 'Learn Vue js',
        pending: true
      },
      {
        description: 'Subscripting in Styde.Net',
        pending: true
      },
      {
        description: 'Recording lesson vue.js',
        pending: false
      }
    ]
  },
  /*
  created:function () {
    this.tasks.forEach(function (task) {
      this.$set(task,'editing',false);
    }.bind(this))
  },*/
  methods: {
    createTask: function () {
      this.tasks.push({
        description: this.new_task,
        pending: true,
        editing: false
      })

      this.new_task = ''
    },
    deleteTask: function(index){
      this.tasks.splice(index, 1)
    },
    deleteCompleteTask: function () {
      this.tasks = this.tasks.filter(function (task) {
        return task.pending
      })
    }
  }
})