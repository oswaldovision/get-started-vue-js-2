Vue.component('app-icon', {
  template: '<span :class="cssClasses" aria-hidden="true"></span>',
  props: ['img'],
  computed: {
    cssClasses: function () {
      return 'glyphicon glyphicon-' + this.img
    }
  }
})

Vue.component('app-task',{
  template: '#task-template',
  props: ['tasks','task','index'],
  data: function(){
    return {
      editing : false,
      draft: ''
    }
  },
  methods:{
    toogleStatus: function () {
      this.task.pending = !this.task.pending
    },
    edit: function () {
      //FIX ME: implement
      /*
      this.tasks.forEach(function (task) {
        task.editing = false
      })
      */

      this.draft = this.task.description

      this.editing = true
    },
    update: function () {
      this.task.description = this.draft
      this.editing = false
    },
    discart: function () {
      this.editing = false;
    },
    remove: function () {
      this.tasks.splice(this.index, 1)
    }
  }
})

var vm = new Vue({
  el: '#app',
  data: {
    new_task: '',
    tasks: [
      {
        description: 'Learn Vue js',
        pending: true
      },
      {
        description: 'Subscripting in Styde.Net',
        pending: true
      },
      {
        description: 'Recording lesson vue.js',
        pending: false
      }
    ]
  },
  /*
  created:function () {
    this.tasks.forEach(function (task) {
      this.$set(task,'editing',false);
    }.bind(this))
  },*/
  methods: {
    createTask: function () {
      this.tasks.push({
        description: this.new_task,
        pending: true,
        editing: false
      })

      this.new_task = ''
    },
    deleteCompleteTask: function () {
      this.tasks = this.tasks.filter(function (task) {
        return task.pending
      })
    }
  }
})