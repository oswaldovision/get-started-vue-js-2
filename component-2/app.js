Vue.component('app-icon', {
  template: '<span :class="cssClasses" aria-hidden="true"></span>',
  props: ['img'],
  computed: {
    cssClasses: function () {
      return 'glyphicon glyphicon-' + this.img
    }
  }
})

Vue.component('app-task',{
  template: '#task-template',
  props: ['tasks','task','index'],
  methods:{
    toogleStatus: function () {
      this.task.pending = !this.task.pending
    },
    edit: function () {
      this.tasks.forEach(function (task) {
        task.editing = false
      })

      this.draft = this.task.description

      this.task.editing = true
    },
    update: function () {
      this.task.description = this.draft
      this.task.editing = false
    },
    discart: function () {
      this.task.editing = false
    },
    remove: function () {
      this.tasks.splice(this.index, 1)
    }
  }
})

var vm = new Vue({
  el: '#app',
  data: {
    draft: '',
    new_task: '',
    tasks: [
      {
        description: 'Learn Vue js',
        pending: true,
        editing: false
      },
      {
        description: 'Subscripting in Styde.Net',
        pending: true,
        editing: false
      },
      {
        description: 'Recording lesson vue.js',
        pending: false,
        editing: false
      }
    ]
  },
  methods: {
    createTask: function () {
      this.tasks.push({
        description: this.new_task,
        pending: true,
        editing: false
      })

      this.new_task = ''
    },
    deleteCompleteTask: function () {
      this.tasks = this.tasks.filter(function (task) {
        return task.pending
      })
    }
  }
})